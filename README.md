ADC to LCD project for the ARM Cortex-M4 architecture uses the STM32F429iDisco board and a 2.4" QVGA TFT LCD that is installed on the board. 2 ADC channels display the value on the graph. Right now it impossible to configure in runtime, only when compile. 
more details about the board at st.com/en/evaluation-tools/32f429idiscovery.html

If necessary, you can reconfigure the project for any STM32F series using CubeMX

Host system:
Windows
Linux (not checked)

To build, lounch and debug the project:
1. Install STM32CubeIDE
https://www.st.com/en/development-tools/stm32cubeide.html
2. After opening the project in the IDE, it should downloaded the necessary STM32Cube FrameWork
3. You need to download an install necessary drivers.
4. Connect the board via the built-in debugger or st-link v2 programmer
5. Load firmware using IDE

What I planned:
1. Add google test for testing independent platform logic.
2. Add Makefiles for add the ability to build using the make
3. Load procedure can be perform via STM tools without IDE
4. Touchscreen menu
5. Add configuring in runtime:
    graph size
    source type (number of ADC)
6. Create Relese 1.0 :)

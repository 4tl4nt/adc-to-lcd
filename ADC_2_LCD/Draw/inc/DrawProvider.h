/*
 * DrawProvider.h
 *
 *  Created on: 8 ѝнв. 2020 г.
 *      Author: Michael�
 */

#ifndef INC_DRAWPROVIDER_H_
#define INC_DRAWPROVIDER_H_

#include <cstdint>

namespace draw {
class Drawable;

class DrawProvider {
public:
	virtual ~DrawProvider() = default;

	virtual void draw() = 0;
	virtual void position(uint16_t xPos, uint16_t yPos) = 0;
	virtual void clear() = 0;
	virtual void fill(uint32_t color) = 0;
	virtual void rect(uint16_t xPos, uint16_t yPos, uint16_t width, uint16_t height, uint32_t color) = 0;
	virtual void fillRect(uint16_t xPos, uint16_t yPos, uint16_t width, uint16_t height, uint32_t color) = 0;
	virtual void line(uint16_t fromX, uint16_t fromY, uint16_t toX, uint16_t toY, uint32_t color) = 0;
	virtual void hLine(uint16_t fromX, uint16_t fromY, uint16_t length, uint32_t color) = 0;
	virtual void vLine(uint16_t fromX, uint16_t fromY, uint16_t length, uint32_t color) = 0;
	virtual void addPicture(Drawable* picture) = 0;
};

} /* namespace draw */

#endif /* INC_DRAWPROVIDER_H_ */

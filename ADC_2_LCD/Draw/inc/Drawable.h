/*
 * Drawable.h
 *
 *  Created on: Jan 7, 2020
 *      Author: Michael
 */

#ifndef INC_DRAWABLE_H_
#define INC_DRAWABLE_H_

#include "DrawProvider.h"

namespace draw
{

class Drawable
{
public:
	Drawable(uint16_t x, uint16_t y, uint16_t width, uint16_t height);
	virtual ~Drawable() = default;
	uint16_t& rX();
	uint16_t& rY();
	uint16_t& rWidth();
	uint16_t& rHeight();
	bool isVisible();
	virtual void draw(draw::DrawProvider *drawProvider) = 0;

private:
    uint16_t mXpos;
    uint16_t mYpos;
    uint16_t mWidth;
    uint16_t mHeight;
	bool mVisible;
};

} /* namespace draw */

#endif /* INC_DRAWABLE_H_ */

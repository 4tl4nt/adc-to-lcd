#include "Drawable.h"

namespace draw
{

Drawable::Drawable(uint16_t x, uint16_t y, uint16_t width, uint16_t height)
    : mXpos(x)
    , mYpos(y)
    , mWidth(width)
    , mHeight(height)
    , mVisible(true)
{
}

uint16_t& Drawable::rX()
{
    return mXpos;
}

uint16_t& Drawable::rY()
{
    return mYpos;
}

uint16_t& Drawable::rWidth()
{
    return mWidth;
}

uint16_t& Drawable::rHeight()
{
    return mHeight;
}

bool Drawable::isVisible()
{
    return mVisible;
}


} /* namespace draw */

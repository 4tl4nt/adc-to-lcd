/*
 * Grid.cpp
 *
 *  Created on: 5 ѝнв. 2020 г.
 *      Author: Михаил
 */

#include "Grid.h"

namespace grid {

Grid::Grid(uint16_t x, uint16_t y, uint16_t width, uint16_t height,
		uint32_t shift, uint32_t lineColor, uint32_t backgroundColor)
	: Drawable(x, y, width, height)
	, mShift(shift)
	, mLineColor(lineColor)
	, mBackgroundColor(backgroundColor)
{
}

void Grid::setShift(uint16_t shift)
{
	mShift = shift;
}

void Grid::setLineColor(uint32_t color)
{
	mLineColor = color;
}

void Grid::setBackgroundColor(uint32_t color)
{
	mBackgroundColor = color;
}

void Grid::draw(draw::DrawProvider *drawProvider)
{
	if(isVisible())
	{
		const auto h = rHeight();
		const auto w = rWidth();
		drawProvider->position(rX(), rY());
		drawProvider->fillRect(0, 0, w, h, mBackgroundColor);
		drawProvider->rect(0, 0, w, h, mLineColor);

		for(int i = 0; i <= w; i += mShift) // vertical lines
		{
			drawProvider->vLine(i, 0, h, mLineColor);
		}
		for(int i = 0; i <= h; i += mShift) // horizontal lines
		{
			drawProvider->hLine(0, i, w, mLineColor);
		}
	}
}

} /* namespace grid */

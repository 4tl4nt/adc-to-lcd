/*
 * Grid.h
 *
 *  Created on: 5 ѝнв. 2020 г.
 *      Author: Михаил
 */

#ifndef INC_GRID_H_
#define INC_GRID_H_

#include <cstdint>

#include "Drawable.h"

namespace grid {

class Grid : public draw::Drawable {
public:
	Grid(uint16_t x, uint16_t y, uint16_t width, uint16_t height,
			uint32_t shift, uint32_t lineColor, uint32_t backgroundColor);
	virtual ~Grid() = default;
	void setShift(uint16_t shift);
	void setLineColor(uint32_t color);
	void setBackgroundColor(uint32_t color);
	virtual void draw(draw::DrawProvider *drawProvider) override final;

private:
	uint16_t mShift;
	uint32_t mLineColor;
	uint32_t mBackgroundColor;
};

} /* namespace grid */

#endif /* INC_GRID_H_ */

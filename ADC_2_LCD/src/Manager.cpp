/*
 * main.cpp
 *
 *  Created on: Jan 1, 2020
 *      Author: Михаил
 */

#include "Manager.h"
#include <stdio.h>
#include <memory>

#include "main.h"
#include "config.h"
#include "BoundaryGraph.h"
#include "CircularGraph.h"
#include "DisplayManager.h"
#include "Grid.h"
#include "RingBuffer.h"
#include "TouchScreen.h"
#include "Memory.h"
#include "FreeRTOSLocker.h"

#include "stm32f429i_discovery_lcd.h"
#include "stm32f429i_discovery_ts.h"

extern ADC_HandleTypeDef hadc1;
extern ADC_HandleTypeDef hadc2;
std::unique_ptr<stm::DisplayManager> displayManager;
std::unique_ptr<stm::TouchScreen> touchScreen;
extern osThreadId displayTaskHandle;
extern osThreadId firstPollingTaskHandle;
extern osThreadId secondPollingTaskHandle;

void firstPollingTask(void const* argument)
{
	auto background = displayManager->getDrawProvider(0);
	auto foreground = displayManager->getDrawProvider(1);

	grid::Grid topGrid(20, 5, 200, 150, 25, LCD_COLOR_BLACK, LCD_COLOR_BLUE);
	background->addPicture(&topGrid);
	std::unique_ptr<rb::RingBuffer<uint16_t>> buf(new rb::RingBuffer<uint16_t>(200));
	std::unique_ptr<locker::Locker> locker(new stm::FreeRTOSLocker());
	graph::BoundaryGraph topGraph(20, 5, 200, 150, LCD_COLOR_GREEN, LCD_COLOR_RED, std::move(buf), std::move(locker));
	foreground->addPicture(&topGraph);

	while (1)
	{
		HAL_GPIO_TogglePin(LD3_GPIO_Port, LD3_Pin);
        HAL_ADC_Start(&hadc1);
        HAL_ADC_PollForConversion(&hadc1,5);
        int value1 = HAL_ADC_GetValue(&hadc1);
    	topGraph.addPoint(value1);
    	osDelay(25);
	}
}

void secondPollingTask(void const * argument)
{
	auto background = displayManager->getDrawProvider(0);
	auto foreground = displayManager->getDrawProvider(1);

	grid::Grid bottomGrid(0, 165, 240, 150, 5, LCD_COLOR_BLUE, LCD_COLOR_GREEN);
	background->addPicture(&bottomGrid);
	std::unique_ptr<rb::RingBuffer<uint16_t>> buf(new rb::RingBuffer<uint16_t>(240));
	std::unique_ptr<locker::Locker> locker(new stm::FreeRTOSLocker());
	graph::CircularGraph bottomGraph(0, 165, 240, 150, LCD_COLOR_WHITE, std::move(buf), std::move(locker));
	foreground->addPicture(&bottomGraph);

	while (true)
	{
		HAL_ADC_Start(&hadc2);
		HAL_ADC_PollForConversion(&hadc2,5);
		int value2 = HAL_ADC_GetValue(&hadc2);
		bottomGraph.addPoint(value2);
		osDelay(10);
	}
}

void displayTask(void const * argument)
{
	//stm::DisplayManager* displayManager = (stm::DisplayManager*)argument; // TODO Do it with C++ style
	while (true)
	{
		/*g_ts.update();
		if(g_ts.getNewState() == a2d::TouchScreen::State::PRESS)
		{
			if (g_display.isVisible())
			{
				g_display.hide();
				g_display.clear(1);
			}
			else
				g_display.show();
		}*/
		displayManager->draw();
		osDelay(1);
	}
}

void initManager()
{
  displayManager.reset(new stm::DisplayManager());
  touchScreen.reset(new stm::TouchScreen());
  displayManager->init(DEFAULT_COLOR_FOR_GRAPH_BACKGROUND, DEFAULT_COLOR_FOR_GRAPH_BACKGROUND, 127, 200);
  displayManager->getDrawProvider(0)->clear();
  displayManager->getDrawProvider(1)->clear();
  displayManager->draw();
  touchScreen->init(displayManager->getDrawProvider(DEFAULT_DRAWING_LAYER));
  displayManager->getDrawProvider(DEFAULT_DRAWING_LAYER)->clear();
  displayManager->draw();
  stm::FreeRTOSLocker::init();
}

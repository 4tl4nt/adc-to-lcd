#pragma once

namespace locker
{

class Locker
{
public:
    Locker() = default;
    virtual ~Locker() = default;
    virtual void lock() = 0;
    virtual void unlock() = 0;
};

} /* namespace locker*/

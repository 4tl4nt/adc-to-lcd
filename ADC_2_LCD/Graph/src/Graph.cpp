/*
 * Graph.h
 *
 *  Created on: Jan 2, 2020
 *      Author: Михаил
 */
#include "Graph.h"

namespace graph {

Graph::Graph(uint16_t width, uint16_t heigth, std::unique_ptr<rb::RingBuffer<uint16_t>> buffer, std::unique_ptr<locker::Locker> locker)
	: mBuffer(std::move(buffer))
	, mLocker(std::move(locker))
	, mWidth(width)
	, mHeight(heigth)
{
}

void Graph::addPoint(uint16_t point)
{
	lock();
	if (mBuffer->isWritable())
	{
		if (mBuffer->isFull())
		{
			uint16_t removedPoint;
			mBuffer->pull(&removedPoint);
		}
		mBuffer->push(point);
	}
	unlock();
}

void Graph::addRangePoints(uint16_t *points, size_t size)
{
	lock();
	if (size > mWidth) return;
	for (size_t i = 0; i < size; i++)
	{
		if (mBuffer->isWritable())
		{
			if (mBuffer->isFull())
			{
				uint16_t removedPoint;
				mBuffer->pull(&removedPoint);
			}
			mBuffer->push(*(points++));
		}
	}
	unlock();
}

void Graph::addAllPoints(uint16_t *points)
{
	lock();
	uint16_t contentIndex = 0;
	mBuffer->clear();
	while (contentIndex < mWidth)
	{
		mBuffer->push(*(points++));
	}
	unlock();
}

void Graph::lock()
{
	mLocker->lock();
}

void Graph::unlock()
{
	mLocker->unlock();
}

} /* namespace graph */

/*
 * BoundaryGraph.h
 *
 *  Created on: Jan 2, 2020
 *      Author: Михаил
 */
#include "BoundaryGraph.h"
#include "config.h"

namespace graph
{

BoundaryGraph::BoundaryGraph(uint16_t x, uint16_t y, uint16_t width, uint16_t height,
		uint32_t graphColor, uint32_t boundaryColor, std::unique_ptr<rb::RingBuffer<uint16_t>> buffer, std::unique_ptr<locker::Locker> locker)
	: Graph(width, height, std::move(buffer), std::move(locker))
	, Drawable(x, y, width, height)
	, mGraphColor(graphColor)
	, mBoundaryColor(boundaryColor)
	, mBoundaryPossition(0)
	, mLastBoundaryPossition(0)
{
}

void BoundaryGraph::draw(draw::DrawProvider *drawProvider)
{
	Graph::lock();
	uint16_t currentY;
	drawProvider->position(rX(), rY());
	drawProvider->vLine(mBoundaryPossition+1, 0, rHeight(), DEFAULT_COLOR_FOR_GRAPH_BACKGROUND);
	while (Graph::mBuffer->pull(&currentY))
	{
		if (currentY >= rHeight()) currentY = rHeight()-1;
		drawProvider->line(mBoundaryPossition, mLastBoundaryPossition, mBoundaryPossition+1, currentY, mGraphColor);
		mLastBoundaryPossition = currentY;
		if (++mBoundaryPossition >= rWidth())
		{
			mBoundaryPossition = 0;
			drawProvider->vLine(mBoundaryPossition, 0, rHeight(), DEFAULT_COLOR_FOR_GRAPH_BACKGROUND);
		}
		drawProvider->vLine(mBoundaryPossition+1, 0, rHeight(), DEFAULT_COLOR_FOR_GRAPH_BACKGROUND);
	}
	drawProvider->vLine(mBoundaryPossition+1, 0, rHeight(), mBoundaryColor);
	Graph::unlock();
}

} /* namespace graph */

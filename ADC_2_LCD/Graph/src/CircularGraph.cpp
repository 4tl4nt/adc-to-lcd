/*
 * CircularGraph.h
 *
 *  Created on: 6 ѝнв. 2020 г.
 *      Author: Михаил
 */

#include "CircularGraph.h"
#include "config.h"

namespace graph
{

CircularGraph::CircularGraph(uint16_t x, uint16_t y, uint16_t width, uint16_t height,
		uint32_t graphColor, std::unique_ptr<rb::RingBuffer<uint16_t>> buffer, std::unique_ptr<locker::Locker> locker)
	: Graph(width, height, std::move(buffer), std::move(locker))
	, Drawable(x, y, width, height)
	, mGraphColor(graphColor)
{
}

void CircularGraph::draw(draw::DrawProvider *drawProvider)
{
	Graph::lock();
	if(isVisible())
	{
		auto const w = rWidth();
		auto const h = rHeight();
		drawProvider->position(rX(), rY());
		drawProvider->fillRect(0, 0, w, h, DEFAULT_COLOR_FOR_GRAPH_BACKGROUND);
		Graph::mBuffer->writable(false);

		auto backPoint = Graph::mBuffer->read(0);
		if (backPoint != NULL && *backPoint > h) *backPoint = h - 1;

		for (auto index = 1; index < rWidth(); index++)
		{
			auto point = Graph::mBuffer->read(index);
			if (point == NULL) break;
			if (*point > h) *point = h - 1;
			drawProvider->line(index-1, *backPoint, index, *point, mGraphColor);
			backPoint = point;
		}
		Graph::mBuffer->writable(true);
	}
	Graph::unlock();
}

} /* namespace graph */

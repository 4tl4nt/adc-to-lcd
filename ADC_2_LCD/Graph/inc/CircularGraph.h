/*
 * CircularGraph.h
 *
 *  Created on: 6 ѝнв. 2020 г.
 *      Author: Михаил
 */

#ifndef INC_CIRCULARGRAPH_H_
#define INC_CIRCULARGRAPH_H_

#include <cstdint>

#include "Drawable.h"
#include "Graph.h"

namespace graph {

class CircularGraph : public Graph, public draw::Drawable {
public:
	CircularGraph(uint16_t x, uint16_t y, uint16_t width, uint16_t height,
			uint32_t graphColor, std::unique_ptr<rb::RingBuffer<uint16_t>> buffer, std::unique_ptr<locker::Locker> locker);
	virtual ~CircularGraph() = default;

	virtual void draw(draw::DrawProvider *drawProvider) override final;

private:
	uint32_t mGraphColor;
};

} /* namespace graph */

#endif /* INC_CIRCULARGRAPH_H_ */

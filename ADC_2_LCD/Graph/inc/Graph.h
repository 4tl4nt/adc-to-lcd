/*
 * Graph.h
 *
 *  Created on: Jan 2, 2020
 *      Author: Михаил
 */

#ifndef INC_GRAPH_H_
#define INC_GRAPH_H_

#include <cstdint>
#include <cstddef>
#include <memory>

#include "RingBuffer.h"
#include "Locker.h"

namespace graph {

class Graph {
public:
	Graph(uint16_t width, uint16_t heigth, std::unique_ptr<rb::RingBuffer<uint16_t>> buffer, std::unique_ptr<locker::Locker> locker);
	virtual ~Graph() = default;
	void addPoint(uint16_t point);
	void addRangePoints(uint16_t *points, size_t size);

	void addAllPoints(uint16_t *points);
	void lock();
	void unlock();

protected:
	std::unique_ptr<rb::RingBuffer<uint16_t>> mBuffer;

private:
	std::unique_ptr<locker::Locker> mLocker;
    const uint16_t mWidth;
    const uint16_t mHeight;
};

} /* namespace graph */

#endif /* INC_GRAPH_H_ */

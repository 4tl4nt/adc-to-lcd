/*
 * BoundaryGraph.h
 *
 *  Created on: Jan 2, 2020
 *      Author: Михаил
 */

#ifndef INC_BOUNDARYGRAPH_H_
#define INC_BOUNDARYGRAPH_H_

#include <cstdint>
#include <memory>

#include "Graph.h"
#include "Drawable.h"

namespace graph
{

class BoundaryGraph : public Graph, public draw::Drawable {
public:
	BoundaryGraph(uint16_t x, uint16_t y, uint16_t width, uint16_t height,
			uint32_t graphColor, uint32_t boundaryColor, std::unique_ptr<rb::RingBuffer<uint16_t>> buffer, std::unique_ptr<locker::Locker> locker);
	virtual ~BoundaryGraph() = default;

	virtual void draw(draw::DrawProvider *drawProvider) override final;

private:
	uint32_t mGraphColor;
    uint32_t mBoundaryColor;
    uint16_t mBoundaryPossition;
    uint16_t mLastBoundaryPossition;
};

} /* namespace graph */

#endif /* INC_BOUNDARYGRAPH_H_ */

/*
 * TouchScreen.h
 *
 *  Created on: Jan 6, 2020
 *      Author: Михаил
 */

#ifndef SRC_TOUCHSCREEN_H_
#define SRC_TOUCHSCREEN_H_

#include "DrawProvider.h"

namespace stm {

class TouchScreen {
public:
	enum State {
		RELEASE = 0,
		PRESS,
		LONGPRESS,
	};

	TouchScreen();
	virtual ~TouchScreen() = default;

	void init(draw::DrawProvider *drawProvider);
	void update();
	State getNewState();

private:
	State mNewState;
	State mCurentState;
	uint32_t mTimePress;
};

} /* namespace stm */

#endif /* SRC_TOUCHSCREEN_H_ */

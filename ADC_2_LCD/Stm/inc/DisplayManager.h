#include "DrawProvider.h"
#include <vector>

#ifndef INC_DISPLAYMANAGER_H_
#define INC_DISPLAYMANAGER_H_

namespace stm {
class Window;

class DisplayManager {
public:
	DisplayManager();
	~DisplayManager();

	draw::DrawProvider* getDrawProvider(uint16_t layer);
	bool init(uint32_t backgroundFontColor, uint32_t foregroundFontColor, uint8_t backgroundTransparency, uint8_t foregroundTransparency);
    void enable();
    void disable();
	void draw();

private:
    std::vector<Window> mDrawProviders;
};

} /* namespace a2d */

#endif /* INC_DISPLAYMANAGER_H_ */

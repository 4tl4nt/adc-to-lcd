#pragma once

#include "Locker.h"
#include "cmsis_os.h"

namespace stm
{

class FreeRTOSLocker : public locker::Locker
{
public:
    FreeRTOSLocker();
    ~FreeRTOSLocker();

    virtual void lock() override final;
    virtual void unlock() override final;

    static void init();
private:
    int mMutexIndex;
};

} /* namespace stm */

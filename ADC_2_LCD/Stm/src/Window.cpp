/*
 * Window.cpp
 *
 *  Created on: Jan 1, 2020
 *      Author: Michael�
 */

#include "stm32f429i_discovery_lcd.h"

#include "Window.h"

namespace stm {

Window::Window(uint16_t layer)
	: mX(0)
	, mY(0)
	, mLayer(layer)
	, mBackgroundColor(LCD_COLOR_BLACK)
{
}

Window::~Window()
{
}

void Window::draw()
{
	for(auto pic : mPictures)
	{
		pic->draw(this);
	}
	CUSTOM_LCD_DrawFB(mLayer);
}

void Window::position(uint16_t xPos, uint16_t yPos)
{
	mX = xPos;
	mY = yPos;
}

void Window::clear()
{
	BSP_LCD_SelectLayer(mLayer);
	BSP_LCD_SetBackColor(mBackgroundColor);
	BSP_LCD_Clear(mBackgroundColor);
}

void Window::fill(uint32_t color)
{
	BSP_LCD_SelectLayer(mLayer);
	BSP_LCD_Clear(color);
}

void Window::rect(uint16_t xPos, uint16_t yPos, uint16_t width, uint16_t height, uint32_t color)
{
	BSP_LCD_SelectLayer(mLayer);
	BSP_LCD_SetTextColor(color);
	BSP_LCD_DrawRect(mX + xPos, mY + yPos, width, height);
}

void Window::fillRect(uint16_t xPos, uint16_t yPos, uint16_t width, uint16_t height, uint32_t color)
{
	BSP_LCD_SelectLayer(mLayer);
	BSP_LCD_SetTextColor(color);
	BSP_LCD_FillRect(mX + xPos, mY + yPos, width, height);
}

void Window::line(uint16_t fromX, uint16_t fromY, uint16_t toX, uint16_t toY, uint32_t color)
{
	BSP_LCD_SelectLayer(mLayer);
	BSP_LCD_SetTextColor(color);
    BSP_LCD_DrawLine(mX + fromX, mY + fromY, mX + toX, mY + toY);
}

void Window::hLine(uint16_t fromX, uint16_t fromY, uint16_t length, uint32_t color)
{
	BSP_LCD_SelectLayer(mLayer);
	BSP_LCD_SetTextColor(color);
    BSP_LCD_DrawHLine(mX + fromX, mY + fromY, length);
}

void Window::vLine(uint16_t fromX, uint16_t fromY, uint16_t length, uint32_t color)
{
	BSP_LCD_SelectLayer(mLayer);
	BSP_LCD_SetTextColor(color);
    BSP_LCD_DrawVLine(mX + fromX, mY + fromY, length);
}

void Window::addPicture(draw::Drawable* picture)
{
	mPictures.push_back(picture);
}

void Window::setBackgroundColor(uint32_t color)
{
	mBackgroundColor = color;
}

} /* namespace stm */

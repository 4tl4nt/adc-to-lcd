/*
 * TouchScreenCalibration.h
 *
 *  Created on: Jan 6, 2020
 *      Author: Михаил
 */

#ifndef INC_TOUCHSCREENCALIBRATION_H_
#define INC_TOUCHSCREENCALIBRATION_H_

#include "DrawProvider.h"

void Touchscreen_Calibration(draw::DrawProvider *drawProvider);
uint8_t IsCalibrationDone(void);

#endif /* INC_TOUCHSCREENCALIBRATION_H_ */

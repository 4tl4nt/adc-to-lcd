#include "Memory.h"
#include "main.h"
#include "cmsis_os.h"

void* operator new(size_t size)
{
    auto ptr = pvPortMalloc(size);
    if (!ptr) Error_Handler();
    return ptr;
}

void operator delete(void* ptr)
{
    vPortFree(ptr);
}

void* operator new[](size_t size)
{
    auto ptr = pvPortMalloc(size);
    if (!ptr) Error_Handler();
    return ptr;
}

void operator delete[](void* ptr)
{
    vPortFree(ptr);
}

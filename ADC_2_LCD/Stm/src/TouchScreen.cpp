/*
 * TouchScreen.cpp
 *
 *  Created on: Jan 6, 2020
 *      Author: Михаил
 */

#include "stm32f429i_discovery_lcd.h"
#include "stm32f429i_discovery_ts.h"
#include "cmsis_os.h"

#include "TouchScreen.h"
#include "TouchScreenCalibration.h"

namespace stm {

TouchScreen::TouchScreen()
	: mNewState(RELEASE)
	, mCurentState(RELEASE)
	, mTimePress(0)
{
}

void TouchScreen::init(draw::DrawProvider *drawProvider)
{
	if(IsCalibrationDone() == 0)
	{
		Touchscreen_Calibration(drawProvider);
		if (BSP_TS_Init(BSP_LCD_GetXSize(), BSP_LCD_GetYSize()) == TS_OK);
	}
	drawProvider->clear();
	osDelay(25);
}

void TouchScreen::update()
{
	TS_StateTypeDef TSstate;
	BSP_TS_GetState(&TSstate);
	if (TSstate.TouchDetected == 1 && mCurentState == RELEASE)
	{
		mTimePress = HAL_GetTick();
		mCurentState = PRESS;
	}
	else if (TSstate.TouchDetected == 0 && mCurentState == PRESS)
	{
		mCurentState = RELEASE;
		if (HAL_GetTick() - mTimePress > 500)
		{
			mNewState = LONGPRESS; // TODO: send notify
		}
		else
		{
			mNewState = PRESS; // TODO: send notify

		}
	}
}

TouchScreen::State TouchScreen::getNewState()
{
	State res = mNewState;
	mNewState = RELEASE;
	return res;
}

} /* namespace stm */

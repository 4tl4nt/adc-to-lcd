#include "DisplayManager.h"
#include "Window.h"

//#include "cmsis_os.h"
#include "stm32f429i_discovery_lcd.h"

namespace stm {

DisplayManager::DisplayManager()
    : mDrawProviders{Window(0), Window(1)}
{
}

DisplayManager::~DisplayManager()
{
}

draw::DrawProvider* DisplayManager::getDrawProvider(uint16_t layer)
{
	return &mDrawProviders.at(layer);
}

bool DisplayManager::init(uint32_t backgroundFontColor, uint32_t foregroundFontColor, uint8_t backgroundTransparency, uint8_t foregroundTransparency)
{
	bool res = false;
	if (BSP_LCD_Init() == LCD_OK)
	{
		// TODO: Add possibility to setup 3rd mLayer of LTDC

		BSP_LCD_LayerDefaultInit(0, LCD_FRAME_BUFFER);
		BSP_LCD_LayerDefaultInit(1, LCD_FRAME_BUFFER + BUFFER_OFFSET);
		BSP_LCD_SetTransparency(0, backgroundTransparency);
		BSP_LCD_SetTransparency(1, foregroundTransparency);
		CUSTOM_LCD_SetFBAdress(0, LCD_FRAME_BUFFER + BUFFER_OFFSET + BUFFER_OFFSET);
		CUSTOM_LCD_SetFBAdress(1, LCD_FRAME_BUFFER + BUFFER_OFFSET + BUFFER_OFFSET + BUFFER_OFFSET);

		mDrawProviders[0].setBackgroundColor(backgroundFontColor);
		mDrawProviders[0].clear();
		mDrawProviders[1].setBackgroundColor(foregroundFontColor);
		mDrawProviders[1].clear();
		res = true;
	}
	return res;
}

void DisplayManager::enable()
{
	BSP_LCD_DisplayOn();
}

void DisplayManager::disable()
{
	BSP_LCD_DisplayOff();
}

void DisplayManager::draw()
{
    for(auto &dp : mDrawProviders)
    {
        dp.draw();
    }
}

} /* namespace stm */

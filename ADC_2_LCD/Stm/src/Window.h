/*
 * Window.h
 *
 *  Created on: Jan 1, 2020
 *      Author: Michael�
 */

#ifndef INC_DISPLAY_H_
#define INC_DISPLAY_H_

#include <cstdint>
#include <vector>

#include "DrawProvider.h"
#include "Drawable.h"

namespace stm
{

class Window : public draw::DrawProvider {
public:
	Window(uint16_t layer);
	virtual ~Window();

	virtual void draw() override final;
	virtual void position(uint16_t xPos, uint16_t yPos) override final;
	virtual void clear() override final;
	virtual void fill(uint32_t color) override final;
	virtual void rect(uint16_t xPos, uint16_t yPos, uint16_t width, uint16_t height, uint32_t color) override final;
	virtual void fillRect(uint16_t xPos, uint16_t yPos, uint16_t width, uint16_t height, uint32_t color) override final;
	virtual void line(uint16_t fromX, uint16_t fromY, uint16_t toX, uint16_t toY, uint32_t color) override final;
	virtual void hLine(uint16_t fromX, uint16_t fromY, uint16_t length, uint32_t color) override final;
	virtual void vLine(uint16_t fromX, uint16_t fromY, uint16_t length, uint32_t color) override final;
	virtual void addPicture(draw::Drawable* picture) override final;

	void setBackgroundColor(uint32_t color);

private:
	uint16_t mX;
	uint16_t mY;
	uint16_t mLayer;
	uint32_t mBackgroundColor;
	std::vector<draw::Drawable*> mPictures;
};

} /* namespace a2d */

#endif /* INC_DISPLAY_H_ */

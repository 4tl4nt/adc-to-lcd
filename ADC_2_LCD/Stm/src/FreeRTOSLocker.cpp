
#include "FreeRTOSLocker.h"

namespace stm
{

namespace
{
struct mutexHolder
{
    osMutexId mutex;
    bool free;
} mutexes[10];
}


FreeRTOSLocker::FreeRTOSLocker()
{
    mMutexIndex = -1;
    for(int i = 0; i < 10; i++)
    {
        if (mutexes[i].free)
        {
            mutexes[i].free = false;
            mMutexIndex = i;
            break;
        }
    }
}

FreeRTOSLocker::~FreeRTOSLocker()
{
    unlock();
    mutexes[mMutexIndex].free = true;
}

void FreeRTOSLocker::lock()
{
    if(osMutexWait(mutexes[mMutexIndex].mutex, 1000) != osOK)
    {
    	while(true)
    	{
    	}
    }
}

void FreeRTOSLocker::unlock()
{
    osMutexRelease(mutexes[mMutexIndex].mutex);
}

void FreeRTOSLocker::init()
{
  osMutexDef(osMutex0);
  mutexes[0] = {osMutexCreate(osMutex(osMutex0)), true};
  osMutexDef(osMutex1);
  mutexes[1] = {osMutexCreate(osMutex(osMutex1)), true};
  osMutexDef(osMutex2);
  mutexes[2] = {osMutexCreate(osMutex(osMutex2)), true};
  osMutexDef(osMutex3);
  mutexes[3] = {osMutexCreate(osMutex(osMutex3)), true};
  osMutexDef(osMutex4);
  mutexes[4] = {osMutexCreate(osMutex(osMutex4)), true};
  osMutexDef(osMutex5);
  mutexes[5] = {osMutexCreate(osMutex(osMutex5)), true};
  osMutexDef(osMutex6);
  mutexes[6] = {osMutexCreate(osMutex(osMutex6)), true};
  osMutexDef(osMutex7);
  mutexes[7] = {osMutexCreate(osMutex(osMutex7)), true};
  osMutexDef(osMutex8);
  mutexes[8] = {osMutexCreate(osMutex(osMutex8)), true};
  osMutexDef(osMutex9);
  mutexes[9] = {osMutexCreate(osMutex(osMutex9)), true};
}

} /* namespace stm */

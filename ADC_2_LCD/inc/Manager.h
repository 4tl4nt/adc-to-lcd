/*
 * a2dMain.h
 *
 *  Created on: Jan 1, 2020
 *      Author: Михаил
 */

#ifndef INC_A2DMAIN_H_
#define INC_A2DMAIN_H_

#ifdef __cplusplus
 extern "C" {
#endif

void displayTask(void const* argument);
void firstPollingTask(void const* argument);
void secondPollingTask(void const* argument);

void initManager();

#ifdef __cplusplus
}
#endif
#endif /* INC_A2DMAIN_H_ */

/*
 * config.h
 *
 *  Created on: 11 янв. 2020 г.
 *      Author: Михаил
 */

#ifndef INC_CONFIG_H_
#define INC_CONFIG_H_

#define DEFAULT_DRAWING_LAYER 1
#define DEFAULT_COLOR_FOR_GRAPH_BACKGROUND 0xFF000000

#endif /* INC_CONFIG_H_ */

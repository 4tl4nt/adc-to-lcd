/*
* Created on: 21.03.2020
*     Author: Michael
*         PC: ASGARD
*        IDE: VS Code
*/
#include <type_traits>

#include "gtest/gtest.h"
#include "RingBuffer.h"
class RingBufferTest : public ::testing::Test
{
protected:
    void SetUp() override
    {
    }

    void TearDown() override
    {
    }
};

TEST_F(RingBufferTest, SimpleWrite)
{
    rb::RingBuffer<int> buf(10);
    EXPECT_EQ(buf.push(13), true);
    EXPECT_EQ(buf.push(14), true);
    EXPECT_EQ(buf.push(15), true);
    EXPECT_EQ(buf.push(92), true);
    EXPECT_EQ(buf.push(6), true);
}

TEST_F(RingBufferTest, SimpleWriteAndRead)
{
    rb::RingBuffer<int> buf(10);
    EXPECT_EQ(buf.push(13), true);
    EXPECT_EQ(buf.push(14), true);
    EXPECT_EQ(buf.push(15), true);
    int element;
    EXPECT_EQ(buf.pull(&element), true);
    EXPECT_EQ(element, 13);
    EXPECT_EQ(buf.pull(&element), true);
    EXPECT_EQ(element, 14);
    EXPECT_EQ(buf.pull(&element), true);
    EXPECT_EQ(element, 15);
}

TEST_F(RingBufferTest, WriteWhileReading)
{
    rb::RingBuffer<int> buf(10);
    EXPECT_EQ(buf.push(13), true);
    EXPECT_EQ(buf.push(14), true);
    EXPECT_EQ(buf.push(15), true);
    int element;
    EXPECT_EQ(buf.pull(&element), true);
    EXPECT_EQ(element, 13);
    EXPECT_EQ(buf.push(-1), true);
    EXPECT_EQ(buf.pull(&element), true);
    EXPECT_EQ(element, 14);
    EXPECT_EQ(buf.pull(&element), true);
    EXPECT_EQ(element, 15);
    EXPECT_EQ(buf.pull(&element), true);
    EXPECT_EQ(element, -1);
}

TEST_F(RingBufferTest, IsFull)
{
    rb::RingBuffer<int> buf(10);
    EXPECT_EQ(buf.isFull(), false);
    for(auto i = 0; i < 10; i++)
    {
        buf.push(i);
    }

    EXPECT_EQ(buf.isFull(), true);
    int element;
    buf.pull(&element);
    EXPECT_EQ(buf.isFull(), false);
}

TEST_F(RingBufferTest, IsEmpty)
{
    rb::RingBuffer<int> buf(10);
    EXPECT_EQ(buf.isEmpty(), true);
    int element = 0;
    buf.push(element);
    EXPECT_EQ(buf.isEmpty(), false);
    buf.pull(&element);
    EXPECT_EQ(buf.isEmpty(), true);
    for(int i = 0; i < 10; i++)
    {
        buf.push(i);
        EXPECT_EQ(buf.isEmpty(), false);
    }

    for(int i = 0; i < 10; i++)
    {
        EXPECT_EQ(buf.isEmpty(), false);
        buf.pull(&element);
    }
    EXPECT_EQ(buf.isEmpty(), true);
}

template<typename T>
void bufferOverFlow()
{
    rb::RingBuffer<T> buf(10);
    for(auto i = 0; i < 1000; i++)
    {
        buf.push(i);
    }

    T element;
    for(auto i = 990; i < 1000; i++)
    {
        EXPECT_EQ(buf.pull(&element), true);
        EXPECT_EQ(element, i);
    }
    EXPECT_EQ(buf.pull(&element), false);
}

TEST_F(RingBufferTest, BufferOverFlow)
{
    bufferOverFlow<short>();
    bufferOverFlow<unsigned short>();
    bufferOverFlow<int>();
    bufferOverFlow<unsigned int>();
    bufferOverFlow<long>();
    bufferOverFlow<unsigned long>();
    bufferOverFlow<long long>();
}

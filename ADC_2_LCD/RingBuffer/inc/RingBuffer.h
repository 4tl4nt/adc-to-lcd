/*
 * RingBuffer.h
 *
 *  Created on: 8 ѝнв. 2020 г.
 *      Author: Михаил
 */

#ifndef SRC_RINGBUFFER_H_
#define SRC_RINGBUFFER_H_

#include <stdint.h>

namespace rb {

template <class T>
class RingBuffer {
public:
	RingBuffer(uint16_t size)
		: mSize(size)
		, mHead(0)
		, mTail(0)
		, mFull(false)
		, mEmpty(true)
		, mWritable(true)
	{
		mBuffer = new T[size]();
	}

	virtual ~RingBuffer()
	{
		delete[] mBuffer;
	}

	bool isEmpty() { return mEmpty; }
	bool isFull() { return mFull; }
	void writable(bool state) { mWritable = state; }
	bool isWritable() { return mWritable; }
	bool push(T element)
	{
		bool res = false;
		if (mWritable)
		{
			mBuffer[mHead++] = element;
			if (mHead == mSize) mHead = 0;
			if (mHead == mTail) mFull = true;
			mEmpty = false;
			res = true;
		}
		return res;
	}

	bool pull(T* element)
	{
		bool res = false;
		if (!isEmpty())
		{
			*element = mBuffer[mTail++];
			if (mTail == mSize) mTail = 0;
			if (mHead == mTail) mEmpty = true;
			mFull = false;
			res = true;
		}
		return res;
	}

	T* read(uint16_t index)// TODO optimize
	{
		T *res = NULL;
		if (!isEmpty())
		{
			int size = mHead < mTail ? mHead + mSize - mTail : mHead - mTail;
			if (!size) size = mSize;
			if (index < size)
			{
				index = index + mTail >= mSize ? index + mTail - mSize : index + mTail;
				res = &mBuffer[index];
			}
		}
		return res;
	}

	void clear()
	{
		mHead = mTail = 0;
		mEmpty = true;
		mFull = false;
	}

private:
	T *mBuffer;
	volatile uint16_t mSize;
	volatile uint16_t mHead;
	volatile uint16_t mTail;
	volatile bool mFull;
	volatile bool mEmpty;
	volatile bool mWritable;
};

} /* namespace rb */

#endif /* SRC_RINGBUFFER_H_ */

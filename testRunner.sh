#!/bin/bash

mkdir build
cd build
cmake .. -DRUN_TESTS_AFTER_BUILD=ON -DCMAKE_INSTALL_PREFIX=../install
cmake  --build . --config Release --target install
